﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Globalization;

namespace MoodleIt.Logic
{
    public static class Utility
    {
        public static string UpperCase(string txt)
        {
            string str;
            txt = txt.ToLower(); //make sure there are no words that are entirely in uppercase
            switch (txt)
            {
                case "angularjs":
                    str = "AngularJS";
                    break;
                case "css":
                    str = "CSS";
                    break;
                case "html":
                    str = "HTML";
                    break;
                default:
                    //For any other string such as student fullName
                    str = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(txt);
                    break;
            }
            return str;
        }
    }
}