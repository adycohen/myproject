﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using MoodleIt.Models;
using MoodleIt.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;


namespace MoodleIt.Logic
{
    public class StudentCourseActions : IDisposable
    {
        private UniversityDbContext db = new UniversityDbContext();
        int courseNumber;

        #region Students
        public void AddStudent(string studentUserNum, string fullName, string email, string phoneNum)
        {
            Student student = new Student()
            {
                StudentUserNumber = studentUserNum,
                FullName = fullName,
                Email = email,
                PhoneNumber = phoneNum
            };
            db.Students.Add(student);
            try
            {
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public IEnumerable<ShowStudents> DisplayAllStudents()
        {
            List<ShowStudents> lstStudents = new List<ShowStudents>();

            //Creating a new list by joining records from different tables
            var lst = (from en in db.Enrollments
                       join st in db.Students on en.StudentId equals st.StudentId
                       join cr in db.Courses on en.CourseId equals cr.CourseId
                       select new
                       {
                           st.StudentId,
                           st.FullName,
                           st.Email,
                           st.PhoneNumber,
                           cr.CourseName,
                           cr.CourseStartDate,
                       }).ToList();

            foreach (var item in lst)
            {
                ShowStudents st = new ShowStudents();
                st.StudentId = item.StudentId;
                st.FullName = item.FullName;
                st.Email = item.Email;
                st.PhoneNumber = item.PhoneNumber;
                st.CourseName = item.CourseName;
                st.CourseStartDate = item.CourseStartDate;

                lstStudents.Add(st);
            }
            return lstStudents;
        }

        //This method is used to delete the user of the student from the users database
        public bool DeleteUserFromDB(int id)
        {
            string studentUserNumber = db.Students.Where(s => s.StudentId == id)
                .Select(s => s.StudentUserNumber).SingleOrDefault();

            using (ApplicationDbContext userDb = new ApplicationDbContext())
            {
                var user = userDb.Users.Find(studentUserNumber);
                userDb.Users.Remove(user);
                try
                {
                    userDb.SaveChanges();
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }

        }

        //This method is used to remove a student from the database
        public bool DeleteStudent(int id)
        {
            var student = db.Students.Where(s => s.StudentId == id).SingleOrDefault();
            db.Students.Remove(student);
            try
            {
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }
        //This method is used to find the studentId by the student user id
        public int GetStudentId(string userId)
        {
            return db.Students.Where(x => x.StudentUserNumber == userId)
                .Select(x => x.StudentId).SingleOrDefault();
        }

        public Student GetStudentById(int? id) //For the DeleteStudent action
        {
            return db.Students.Where(s => s.StudentId == id).SingleOrDefault();
        }


        public IEnumerable<ContactList> ContactList(int studentId)
        {
            //The list to return
            List<ContactList> lst = new List<ContactList>();

            //Find all the courses Id's of the current student
            var courseID = db.Enrollments.Where(e => e.StudentId == studentId)
                .Select(e => e.CourseId).ToList();

            foreach (var item in courseID)
            {
                //Students from the same course
                var student = (from e in db.Enrollments
                               join s in db.Students on e.StudentId equals s.StudentId
                               join c in db.Courses on e.CourseId equals c.CourseId
                               where c.CourseId == item
                               select new
                               {
                                   s.StudentId,
                                   s.FullName,
                                   s.Email,
                                   s.PhoneNumber,
                                   c.CourseName,
                                   c.CourseNumber,
                                   c.CourseId,
                                   e.EnrollmentId
                               }).ToList();

                foreach (var x in student)
                {
                    ContactList obj = new ContactList();
                    obj.StudentId = x.StudentId;
                    obj.FullName = x.FullName;
                    obj.Email = x.Email;
                    obj.PhoneNumber = x.PhoneNumber;
                    obj.EnrollmentId = x.EnrollmentId;
                    obj.CourseId = item;
                    obj.CourseName = x.CourseName;
                    obj.CourseNumber = x.CourseNumber;

                    lst.Add(obj);
                }
            }
            //In order to remove the current student from the contact list
            lst.RemoveAll((x) => x.StudentId == studentId);

            return lst;
        }
        #endregion
        #region Enrollments

        public IEnumerable<ShowEnrollments> ShowAllEnrollments()
        {
            List<ShowEnrollments> lstEnrollments = new List<ShowEnrollments>();

            var lst = (from en in db.Enrollments
                       join st in db.Students on en.StudentId equals st.StudentId
                       join cr in db.Courses on en.CourseId equals cr.CourseId
                       select new
                       {
                           st.StudentId,
                           st.FullName,
                           st.Email,
                           st.PhoneNumber,
                           cr.CourseName,
                           cr.CourseStartDate
                       }).ToList();

            foreach (var item in lst)
            {
                ShowEnrollments se = new ShowEnrollments();
                se.StudentId = item.StudentId;
                se.FullName = item.FullName;
                se.Email = item.Email;
                se.PhoneNumber = item.PhoneNumber;
                se.CourseName = item.CourseName;
                se.CourseStartDate = item.CourseStartDate;

                lstEnrollments.Add(se);
            }
            return lstEnrollments;
        }

        //This method is called when a student register and choose his course
        public void AddEnrollment(int studentId, int courseID)
        {
            Enrollment enrollment = new Enrollment()
            {
                StudentId = studentId,
                CourseId = courseID
            };
            db.Enrollments.Add(enrollment);
            try
            {
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        #endregion
        #region Add a New Course

        //Find the courseId by course name & course start date
        public int GetCourseId(Courses courseName, DateTime startDate)
        {
            int courseId = (from c in db.Courses
                            where c.CourseName == courseName &&
                            c.CourseStartDate == startDate
                            select c.CourseId).FirstOrDefault();
            return courseId;
        }

        public void CreateNewCourse(Courses courseName, DateTime startDate)
        {
            Course newCourse = new Course();
            newCourse.CourseName = courseName;
            newCourse.CourseNumber = (int)courseName;
            newCourse.CourseStartDate = startDate;

            db.Courses.Add(newCourse);
            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public IEnumerable<Course> ShowAllCourses()
        {
            return db.Courses.ToList();
        }

        #endregion
        #region Assignments
        public void AddAssignment(Courses courseName, DateTime startDate, string title,
            string description, DateTime deadLine)
        {
            Assignment assignment = new Assignment()
            {
                CourseId = db.Courses.Where(x => x.CourseName == courseName && x.CourseStartDate == startDate)
                .Select(x => x.CourseId).SingleOrDefault(),
                AssignmentTitle = title,
                AssignmentDescription = description,
                DeadLine = deadLine
            };
            db.Assignments.Add(assignment);

            try
            {
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        //This method is called in order to find the desired assignment to edit
        public EditAssignment GetAssignmentByIdToEdit(int? id)
        {
            var x = (from a in db.Assignments
                     join c in db.Courses on a.CourseId equals c.CourseId
                     where a.AssignmentId == id
                     select new
                     {
                         c.CourseId,
                         a.AssignmentId,
                         c.CourseName,
                         c.CourseStartDate,
                         a.AssignmentTitle,
                         a.AssignmentDescription,
                         a.DeadLine
                     }).SingleOrDefault();

            EditAssignment obj = new EditAssignment();
            obj.AssignmentId = x.AssignmentId;
            obj.CourseId = x.CourseId;
            obj.CourseName = x.CourseName;
            obj.StartDate = x.CourseStartDate;
            obj.Title = x.AssignmentTitle;
            obj.Description = x.AssignmentDescription;
            obj.DeadLine = x.DeadLine;

            return obj;
        }

        //This method is used to update an assignment
        public void EditAssignment(int? id, Courses courseName, DateTime startDate, string title, string desc,
            DateTime deadLine)
        {
            var update = db.Assignments.Where(a => a.AssignmentId == id).SingleOrDefault();
            update.CourseId = db.Courses.Where(c => c.CourseName == courseName && c.CourseStartDate == startDate)
                .Select(c => c.CourseId).SingleOrDefault();
            update.AssignmentTitle = title;
            update.AssignmentDescription = desc;
            update.DeadLine = deadLine;
            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        //This method is used to find a specific assignment
        public Assignment GetAssignmentById(int? id)
        {
            return db.Assignments.Where(s => s.AssignmentId == id).SingleOrDefault();
        }

        public bool DeleteAssignment(int? id)
        {
            var assignment = db.Assignments.Where(s => s.AssignmentId == id).SingleOrDefault();
            db.Assignments.Remove(assignment);
            try
            {
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }

        public IEnumerable<ShowAssignments> ShowAssignments()
        {
            List<ShowAssignments> showAssignments = new List<ShowAssignments>();

            //Pick data from two tables into a temporary list
            var assignmentList = (from a in db.Assignments
                                  join cr in db.Courses on a.CourseId equals cr.CourseId
                                  select new
                                  {
                                      a.AssignmentId,
                                      cr.CourseName,
                                      cr.CourseNumber,
                                      cr.CourseStartDate,
                                      a.AssignmentTitle,
                                      a.AssignmentDescription,
                                      a.DeadLine
                                  }).ToList();

            //Take the properties and get it into the list of type ShowAssignments
            foreach (var item in assignmentList)
            {
                ShowAssignments sa = new ShowAssignments();
                sa.AssignmentId = item.AssignmentId;
                sa.CourseName = item.CourseName;
                sa.CourseNumber = item.CourseNumber;
                sa.StartDate = item.CourseStartDate;
                sa.AssignmentTitle = item.AssignmentTitle;
                sa.AssignmentDesc = item.AssignmentDescription;
                sa.DeadLine = item.DeadLine;

                showAssignments.Add(sa);
            }
            return showAssignments;
        }

        //Display the assignments to the students
        public IEnumerable<ShowAssignments> ShowStudentAssignments(int? studId)
        {
            List<ShowAssignments> showAssignments = new List<ShowAssignments>();

            var assignmentList = (from a in db.Assignments
                                  join cr in db.Courses on a.CourseId equals cr.CourseId
                                  join en in db.Enrollments on cr.CourseId equals en.CourseId
                                  join st in db.Students on en.StudentId equals st.StudentId
                                  where en.StudentId == studId //Only for the current student
                                  select new
                                  {
                                      a.AssignmentId,
                                      cr.CourseName,
                                      cr.CourseNumber,
                                      cr.CourseStartDate,
                                      a.AssignmentTitle,
                                      a.AssignmentDescription,
                                      a.DeadLine,
                                      cr.CourseId
                                  }).ToList();

            foreach (var item in assignmentList)
            {
                ShowAssignments sa = new ShowAssignments();
                sa.AssignmentId = item.AssignmentId;
                sa.CourseName = item.CourseName;
                sa.CourseNumber = item.CourseNumber;
                sa.StartDate = item.CourseStartDate;
                sa.AssignmentTitle = item.AssignmentTitle;
                sa.AssignmentDesc = item.AssignmentDescription;
                sa.DeadLine = item.DeadLine;
                sa.CourseId = item.CourseId;

                showAssignments.Add(sa);
            }
            return showAssignments;
        }
        #endregion
        #region  Homework

        //This method is used to save the uploaded homework to the database
        public void UploadHomework(int assignmentId, int studentId, string filePath)
        {
            Homework hw = new Homework()
            {
                AssignmentId = assignmentId,
                StudentId = studentId,
                FilePath = filePath,
                SendDate = DateTime.Today,
                HomeworkSent = true
            };
            db.Homeworks.Add(hw);
            try
            {
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }

        //This method is used to find the course name of a specific assignment
        public string GetCourseNameByAssignmentId(int assignmentId)
        {
            var courseName = (from a in db.Assignments
                              join c in db.Courses on a.CourseId equals c.CourseId
                              select new
                              {
                                  a.AssignmentId,
                                  c.CourseName
                              }).ToList();

            string name = courseName.Where(x => x.AssignmentId == assignmentId)
                .Select(x => x.CourseName).SingleOrDefault().ToString();
            return name;
        }

        //This method is used to find the course start date of a specific assignment
        public string GetCourseStartDateByAssignmentId(int assignmentId)
        {
            var startDate = (from a in db.Assignments
                             join c in db.Courses on a.CourseId equals c.CourseId
                             select new
                             {
                                 a.AssignmentId,
                                 c.CourseStartDate
                             }).ToList();

            string date = startDate.Where(x => x.AssignmentId == assignmentId)
                .Select(x => x.CourseStartDate).SingleOrDefault().ToString("ddMMyyyy");

            return date;
        }

        //This method is used to display the student name on the page in which his homework are shown
        public string GetUserFullNameById(int? studId)
        {
            string fullName = db.Students.Where(s => s.StudentId == studId)
                .Select(s => s.FullName).SingleOrDefault().ToString();
            return fullName;
        }

        //This method is used to find the student name (without space) and add it to the name of the file, when uploaded
        public string GetUserFullNameByIdWithoutSpace(int? studId)
        {
            string fullName = db.Students.Where(s => s.StudentId == studId)
                .Select(s => s.FullName).SingleOrDefault().ToString();
            string full_name = fullName.Replace(" ", string.Empty);
            return full_name;
        }



        //The purpose of the next method is to display the "homework upload status" to the student
        public List<ShowHomework> ShowStudentHomework(int? studId)
        {

            //Find all the assignments that are relevant to the current student
            var studAssignments = this.ShowStudentAssignments(studId);

            //Find all homework uploaded by the current student
            var studHW = db.Homeworks.Where(h => h.StudentId == studId).ToList();

            List<ShowHomework> HwList = new List<ShowHomework>();
            foreach (var assignment in studAssignments)
            {
                ShowHomework sh = new ShowHomework();

                sh.AssignmentId = assignment.AssignmentId;
                sh.DeadLine = assignment.DeadLine;

                foreach (var hw in studHW)
                {
                    if (assignment.AssignmentId == hw.AssignmentId)
                    {
                        sh.AssignmentId = assignment.AssignmentId;
                        sh.DeadLine = assignment.DeadLine;
                        sh.HomeworkSent = hw.HomeworkSent;
                        sh.SendDate = hw.SendDate;
                        sh.HomeworkChecked = hw.HomeworkChecked;
                        sh.CheckDate = hw.CheckDate;
                        sh.Grade = hw.Grade;
                        sh.CourseId = assignment.CourseId;
                    }
                }
                HwList.Add(sh);
            }
            return HwList;
        }

        public List<ShowHomework> ShowStudentHomeworkToAdmin(int? studId)
        {
            //Find all the assignments that are relevant to the current student
            var studAssignments = this.ShowStudentAssignments(studId);

            //Find all homework that has already been uploaded by the current student
            var studHW = db.Homeworks.Where(h => h.StudentId == studId).ToList();

            List<ShowHomework> HwList = new List<ShowHomework>();
            foreach (var assignment in studAssignments)
            {
                ShowHomework sh = new ShowHomework();
                sh.AssignmentId = assignment.AssignmentId;
                sh.DeadLine = assignment.DeadLine;

                foreach (var hw in studHW)
                {
                    if (assignment.AssignmentId == hw.AssignmentId)
                    {
                        sh.AssignmentId = assignment.AssignmentId;
                        sh.DeadLine = assignment.DeadLine;
                        sh.HomeworkSent = hw.HomeworkSent;
                        sh.SendDate = hw.SendDate;
                        sh.HomeworkChecked = hw.HomeworkChecked;
                        sh.CheckDate = hw.CheckDate;
                        sh.Grade = hw.Grade;
                        sh.HomeworkId = hw.HomeworkId;
                        sh.StudentId = hw.StudentId;
                        sh.CourseId = assignment.CourseId;
                    }
                }
                HwList.Add(sh);
            }
            return HwList.Where(h => h.HomeworkSent == true).ToList(); //Display only the homework that has already been uploaded.
        }

        //This method is used to check whether a homework file has already been uploaded
        public bool HomeworkExists(int studentId, int assignmentId)
        {
            int homeworkId = (from h in db.Homeworks
                              where h.StudentId == studentId
                              where h.AssignmentId == assignmentId
                              select h.HomeworkId).SingleOrDefault();
            if (homeworkId == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        //This method is used to delete a homework file (if it exsists) before uploading a new one 
        public void DeleteHomework(int studentId, int assignmentId)
        {
            var hw = db.Homeworks.Where(h => h.StudentId == studentId)
                .Where(h => h.AssignmentId == assignmentId).SingleOrDefault();
            db.Homeworks.Remove(hw);
            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        //THE FOLLOWING METHODS ARE USED IN THE ADMIN CONTROLLER (in the DownloadHomework action):
        public string GetCourseNameByHomeworkId(int homeworkId)
        {
            var courseName = (from a in db.Assignments
                              join h in db.Homeworks on a.AssignmentId equals h.AssignmentId
                              join c in db.Courses on a.CourseId equals c.CourseId
                              select new
                              {
                                  h.HomeworkId,
                                  c.CourseName
                              }).ToList();

            string name = courseName.Where(x => x.HomeworkId == homeworkId)
                .Select(x => x.CourseName).SingleOrDefault().ToString();
            return name;
        }

        public string GetCourseStartDateByHomeworkId(int homeworkId)
        {
            var startDate = (from a in db.Assignments
                             join h in db.Homeworks on a.AssignmentId equals h.AssignmentId
                             join c in db.Courses on a.CourseId equals c.CourseId
                             select new
                             {
                                 h.HomeworkId,
                                 c.CourseStartDate
                             }).ToList();

            string date = startDate.Where(x => x.HomeworkId == homeworkId)
                .Select(x => x.CourseStartDate).SingleOrDefault().ToString("ddMMyyyy");

            return date;
        }

        public string GetHomeworkSendDate(int homeworkId)
        {
            return db.Homeworks.Where(h => h.HomeworkId == homeworkId)
                .Select(h => h.SendDate).SingleOrDefault().ToString("ddMMyyyy");
        }

        public string GetFileName(int homeworkId)
        {
            return db.Homeworks.Where(h => h.HomeworkId == homeworkId)
                .Select(h => h.FilePath).SingleOrDefault().ToString();
        }

        public void UpdateGrade(int? homeworkId, DateTime checkDate, int? grade, int studentId, int courseId)
        {
            var hw = db.Homeworks.Where(h => h.HomeworkId == homeworkId).SingleOrDefault();
            hw.HomeworkChecked = true;
            hw.CheckDate = checkDate;
            hw.Grade = grade;

            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }      

        }

        #endregion
        #region DropDownLists
        public List<string> GetCoursesNames()
        {
            var coursesNames = (from m in db.Courses
                                orderby m.CourseName
                                select m.CourseName).ToList();

            List<string> xx = new List<string>();
            List<string> lst = new List<string>();
            foreach (var item in coursesNames)
            {
                string s = item.ToString();
                xx.Add(s);

            }

            foreach (var item in xx)
            {
                if (!lst.Contains(item))
                {
                    lst.Add(item);
                }
            }

            return lst;
        }

        //This method is used to find all the courses of the desired student
        public List<string> GetCoursesNamesById(int studentId)
        {
            var courseId = db.Enrollments.Where(e => e.StudentId == studentId).Select(e => e.CourseId).ToList();

            List<string> lst = new List<string>();
            foreach (var item in courseId)
            {

                var coursesNames = (from m in db.Courses
                                    where m.CourseId == item
                                    orderby m.CourseName
                                    select m.CourseName).ToList();

                List<string> xx = new List<string>();
                foreach (var j in coursesNames)
                {
                    string s = j.ToString();
                    xx.Add(s);
                }
                foreach (var i in xx)
                {
                    if (!lst.Contains(i))
                    {
                        lst.Add(i);
                    }
                }
            }


            return lst;
        }

        public List<string> GetCoursesDates()
        {
            var coursesDates = (from c in db.Courses
                                orderby c.CourseStartDate
                                select c.CourseStartDate).ToList();

            var stringDates = coursesDates.Select(d => d.ToString("dd/MM/yyyy"));

            List<string> lst = new List<string>();
            foreach (var item in stringDates)
            {
                if (lst.Contains(item))
                {
                    break;
                }
                else
                {
                    lst.Add(item);
                }
            }

            return lst;
        }

        //FOR REGISTER DROPDOWNLIST 
        public List<Course> GetCourseName()
        {
            List<Course> list = new List<Course>();
            List<Course> x = db.Courses.ToList();
            foreach (var item in x)
            {
                if (!list.Any(a => a.CourseName == item.CourseName))
                {
                    list.Add(item);
                }
            }

            return list;
        }

        public List<Course> GetCourseDate(Courses name)
        {
            List<Course> list = db.Courses.Where(x => x.CourseName == name).ToList();
            return list;
        }

        #endregion
        #region Announcements

        public void AddAnnouncement(Courses courseName, DateTime startDate, string title, string description)
        {
            Announcement announcment = new Announcement()
            {
                CourseId = db.Courses.Where(x => x.CourseName == courseName && x.CourseStartDate == startDate)
                .Select(x => x.CourseId).SingleOrDefault(),
                AnnouncementTitle = Utility.UpperCase(title),
                AnnouncementDescription = description,
                Date = DateTime.Today
            };
            db.Announcements.Add(announcment);

            try
            {
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        public IEnumerable<ShowAnnouncements> ShowAnnoucements()
        {
            List<ShowAnnouncements> showAnnoucements = new List<ShowAnnouncements>();

            //Pick data from two tables into a temporary list
            var announcementList = (from a in db.Announcements
                                    join cr in db.Courses on a.CourseId equals cr.CourseId
                                    select new
                                    {
                                        a.AnnouncementId,
                                        cr.CourseName,
                                        cr.CourseNumber,
                                        cr.CourseStartDate,
                                        a.AnnouncementTitle,
                                        a.AnnouncementDescription,
                                        a.Date
                                    }).ToList();

            //Take the properties and get it into the list of type ShowAssignments
            foreach (var item in announcementList)
            {
                ShowAnnouncements sa = new ShowAnnouncements();
                sa.AnnouncementId = item.AnnouncementId;
                sa.CourseName = item.CourseName;
                sa.CourseNumber = item.CourseNumber;
                sa.StartDate = item.CourseStartDate;
                sa.AnnouncementTitle = item.AnnouncementTitle;
                sa.AnnouncementDescription = item.AnnouncementDescription;
                sa.Date = item.Date;

                showAnnoucements.Add(sa);
            }
            return showAnnoucements;
        }
        public ShowAnnouncements FindAnnouncementById(int? id)
        {

            var announcement = (from a in db.Announcements
                                join cr in db.Courses on a.CourseId equals cr.CourseId
                                where a.AnnouncementId == id
                                select new ShowAnnouncements
                                {
                                    AnnouncementId = a.AnnouncementId,
                                    CourseNumber = cr.CourseNumber,
                                    CourseName = cr.CourseName,
                                    StartDate = cr.CourseStartDate,
                                    AnnouncementTitle = a.AnnouncementTitle,
                                    AnnouncementDescription = a.AnnouncementDescription,
                                    Date = a.Date
                                }).SingleOrDefault();

            return announcement;
        }

        public void EditAnnouncement(int id, string title, string desc)
        {
            var update = db.Announcements.Where(a => a.AnnouncementId == id).SingleOrDefault();
            update.AnnouncementDescription = desc;
            update.AnnouncementTitle = title;

            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public bool DeleteAnnouncement(int id)
        {
            var announcement = db.Announcements.Where(s => s.AnnouncementId == id).SingleOrDefault();
            db.Announcements.Remove(announcement);
            try
            {
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public List<Announcement> ShowCourseAnnouncements(int studentId)
        {
            List<Announcement> list = new List<Announcement>();
            var courseId = db.Enrollments.Where(e => e.StudentId == studentId)
                .Select(e => e.CourseId).ToList();

            foreach (var item in courseId)
            {
                var announcements = db.Announcements.Where(a => a.CourseId == item).ToList();
                foreach (var i in announcements)
                {
                    Announcement obj = new Announcement();
                    obj.AnnouncementId = i.AnnouncementId;
                    obj.AnnouncementTitle = i.AnnouncementTitle;
                    obj.AnnouncementDescription = i.AnnouncementDescription;
                    obj.CourseId = i.CourseId;
                    obj.Date = i.Date;

                    list.Add(obj);
                }
            }

            return list;
        }

        public int GetCourseIdByCourseName(string courseName, int studentId)
        {
            Courses x;
            switch (courseName)
            {
                case "AngularJS":
                    Enum.TryParse("AngularJS", out x);
                    break;
                case "CSS":
                    Enum.TryParse("CSS", out x);
                    break;
                default:
                    Enum.TryParse("HTML", out x);
                    break;
            }
            var courseId = (from c in db.Courses
                            join e in db.Enrollments on c.CourseId equals e.CourseId
                            where c.CourseName == x && e.StudentId == studentId
                            select c.CourseId).FirstOrDefault();
            return courseId;
        }

        #endregion

        #region Check DB

        public bool IsStudentDatabaseEmpty()
        {
            if (db.Students == null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion
        #region Dispose

        public void Dispose()
        {
            if (db != null)
            {
                db.Dispose();
                db = null;
            }
        }
        #endregion


    }
}