﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PagedList;
using Newtonsoft.Json;


using MoodleIt.ViewModels;
using MoodleIt.Models;
using MoodleIt.Logic;
using System.IO;

namespace MoodleIt.Controllers
{
    [Authorize(Roles = "Admin")] //Only the admin user can access this controller
    public class AdminController : Controller
    {
        private UniversityDbContext db = new UniversityDbContext();
        StudentCourseActions sca = new StudentCourseActions();

        #region Assignments
        
        // GET: /Admin/
        //Display all the assignments to the teacher (admin)
        public ActionResult Index(string sortOrder, string searchText,
            string currentFilter, int? page)
        {
            ViewBag.CurrentSort = sortOrder;

            //Ternary Operator- short way of "if"
            ViewBag.AssignmentIdSort =
                sortOrder == "AssignmentIdAsc" ? "AssignmentIdDesc" : "AssignmentIdAsc";
            ViewBag.CourseNumberSort =
                sortOrder == "CourseNumberAsc" ? "CourseNumberDesc" : "CourseNumberAsc";
            ViewBag.CourseNameSort =
                sortOrder == "CourseNameAsc" ? "CourseNameDesc" : "CourseNameAsc";
            ViewBag.StartDateSort =
                sortOrder == "StartDateAsc" ? "StartDateDesc" : "StartDateAsc";
            ViewBag.AssignmentTitleSort =
                sortOrder == "AssignmentTitleAsc" ? "AssignmentTitleDesc" : "AssignmentTitleAsc";
            ViewBag.DeadLineSort =
                sortOrder == "DeadLineAsc" ? "DeadLineDesc" : "DeadLineAsc";

            if (searchText != null)
            {
                page = 1;
            }
            else
            {
                searchText = currentFilter;
            }
            ViewBag.CurrentFilter = searchText; //Saves the filter and sorting while paging

            var assignments = sca.ShowAssignments();

            if (!string.IsNullOrEmpty(searchText))
            {
                string searcTextUppercase = Utility.UpperCase(searchText);
                assignments = assignments.Where(s =>
                    s.CourseNumber.ToString().Contains(searchText) ||
                    s.CourseName.ToString().Contains(searcTextUppercase) ||
                    s.StartDate.ToString().Contains(searchText) ||
                    s.AssignmentTitle.ToString().Contains(searchText) ||
                    s.DeadLine.ToString().Contains(searchText)).ToList();
            }

            switch (sortOrder)
            {
                case "CourseNumberAsc":
                    assignments = assignments.OrderBy(s => s.CourseNumber);
                    break;
                case "CourseNumberDesc":
                    assignments = assignments.OrderByDescending(s => s.CourseNumber);
                    break;
                case "CourseNameAsc":
                    assignments = assignments.OrderBy(s => s.CourseName);
                    break;
                case "CourseNameDesc":
                    assignments = assignments.OrderByDescending(s => s.CourseName);
                    break;
                case "StartDateAsc":
                    assignments = assignments.OrderBy(s => s.StartDate);
                    break;
                case "StartDateDesc":
                    assignments = assignments.OrderByDescending(s => s.StartDate);
                    break;
                case "AssignmentTitleAsc":
                    assignments = assignments.OrderBy(s => s.AssignmentTitle);
                    break;
                case "AssignmentTitleDesc":
                    assignments = assignments.OrderByDescending(s => s.AssignmentTitle);
                    break;
                case "DeadLineAsc":
                    assignments = assignments.OrderBy(s => s.DeadLine);
                    break;
                case "DeadLineDesc":
                    assignments = assignments.OrderByDescending(s => s.DeadLine);
                    break;
                default:
                    assignments = assignments.OrderBy(s => s.AssignmentId);
                    break;
            }

            int pageSize = 5;
            int pageNumber = page ?? 1;
            return View(assignments.ToPagedList(pageNumber, pageSize));
        }


        // GET: /Admin/Create
        public ActionResult Create()
        {
            ViewBag.CourseNames = new SelectList(sca.GetCourseName(), "CourseName", "CourseName".ToString());
            return View();
        }

        // POST: /Admin/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CourseName,StartDate,Title,Description,DeadLine")] 
            CreateAssignment assignment)
        {
            //Add a new assignment
            if (ModelState.IsValid)
            {
                sca.AddAssignment(assignment.CourseName, assignment.StartDate,
                    assignment.Title, assignment.Description,
                    assignment.DeadLine);

                return RedirectToAction("Index");
            }

            return View(assignment);
        }

        [HttpPost]
        public ActionResult GetCourseStartDate(string courseName)
        {
            //Parse string to Enum:
            Courses x;
            switch (courseName)
            {
                case "AngularJS":
                    Enum.TryParse("AngularJS", out x);
                    break;
                case "CSS":
                    Enum.TryParse("CSS", out x);
                    break;
                default:
                    Enum.TryParse("HTML", out x);
                    break;
            }
            List<Course> dates = sca.GetCourseDate(x);
            var list = dates.Select(d => d.CourseStartDate).ToList();

            ViewBag.DateOptions = new SelectList(list, "CourseStartDate", "CourseStartDate");

            string result = JsonConvert.SerializeObject(list);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        // GET: /Admin/Edit/5
        public ActionResult Edit(int? id)
        {
            //In the Edit view, the admin should select a course name:
            ViewBag.CourseNames = new SelectList(sca.GetCourseName(), "CourseName", "CourseName".ToString());
            var edit = sca.GetAssignmentByIdToEdit(id);

            return View(edit);
        }

        // POST: /Admin/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CourseName,StartDate,Title,Description,DeadLine")]
            CreateAssignment assignment, int? AssignmentId)
        {
            if (ModelState.IsValid)
            {
                sca.EditAssignment(AssignmentId, assignment.CourseName, assignment.StartDate, assignment.Title,
                    assignment.Description, assignment.DeadLine);
                return RedirectToAction("Index");
            }
            ViewBag.CourseNames = new SelectList(sca.GetCourseName(), "CourseName", "CourseName".ToString());

            return View();
        }

        // GET: /Admin/Details/5
        public ActionResult Details(int? id)
        {
            var assignment = sca.GetAssignmentById(id);
            return View(assignment);
        }

        // GET: /Admin/DeleteAssignment/5
        public ActionResult DeleteAssignment(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var assignment = sca.GetAssignmentById(id);
            if (assignment == null)
            {
                return HttpNotFound();
            }
            return View(assignment);
        }

        // POST: /Admin/DeleteAssignment/5
        [HttpPost, ActionName("DeleteAssignment")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteAssignmentConfirmed(int? id)
        {
            if (sca.DeleteAssignment(id))
            {
                return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("DeleteAssignment", id);
            }
        }
        #endregion

        #region Courses

        // GET: /Admin/Courses
        //Display the list of all the courses:
        public ActionResult Courses(string sortOrder, string searchText,
            string currentFilter, int? page)
        {
            ViewBag.CurrentSort = sortOrder;

            //Ternary Operator- short way of "if"
            ViewBag.CourseIdSort =
                sortOrder == "CourseIdAsc" ? "CourseIdDesc" : "CourseIdAsc";
            ViewBag.CourseNumberSort =
                sortOrder == "CourseNumberAsc" ? "CourseNumberDesc" : "CourseNumberAsc";
            ViewBag.CourseNameSort =
                sortOrder == "CourseNameAsc" ? "CourseNameDesc" : "CourseNameAsc";
            ViewBag.CourseStartDateSort =
                sortOrder == "CourseStartDateAsc" ? "CourseStartDateDesc" : "CourseStartDateAsc";

            if (searchText != null)
            {
                page = 1;
            }
            else
            {
                searchText = currentFilter;
            }
            ViewBag.CurrentFilter = searchText; //Saves the filter and sorting while paging

            var courseList = sca.ShowAllCourses();

            if (!string.IsNullOrEmpty(searchText))
            {
                string searcTextUppercase = Utility.UpperCase(searchText);
                courseList = courseList.Where(s =>
                    s.CourseNumber.ToString().Contains(searchText) ||
                    s.CourseName.ToString().Contains(searcTextUppercase) ||
                    s.CourseStartDate.ToString().Contains(searchText)).ToList();
            }

            switch (sortOrder)
            {
                case "CourseNumberAsc":
                    courseList = courseList.OrderBy(s => s.CourseNumber);
                    break;
                case "CourseNumberDesc":
                    courseList = courseList.OrderByDescending(s => s.CourseNumber);
                    break;
                case "CourseNameAsc":
                    courseList = courseList.OrderBy(s => s.CourseName);
                    break;
                case "CourseNameDesc":
                    courseList = courseList.OrderByDescending(s => s.CourseName);
                    break;
                case "CourseStartDateAsc":
                    courseList = courseList.OrderBy(s => s.CourseStartDate);
                    break;
                case "CourseStartDateDesc":
                    courseList = courseList.OrderByDescending(s => s.CourseStartDate);
                    break;
                default:
                    courseList = courseList.OrderBy(s => s.CourseId);
                    break;
            }

            int pageSize = 5;
            int pageNumber = page ?? 1;
            return View(courseList.ToPagedList(pageNumber, pageSize));
        }

        // GET: /Admin/CreateNewCourse
        public ActionResult CreateNewCourse()
        {
            //The admin should select a course name from the list, and then he choose the desired date to start the course.
            ViewBag.CourseNames = new SelectList(sca.GetCourseName(), "CourseName", "CourseName".ToString());

            return View();
        }

        // POST: /Admin/CreateNewCourse
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateNewCourse([Bind(Include = "CourseName,CourseStartDate")] 
            Course course)
        {
            if (ModelState.IsValid)
            {
                sca.CreateNewCourse(course.CourseName, course.CourseStartDate);

                return RedirectToAction("Courses");
            }

            return View(course);
        }

        #endregion

        #region Enrollments (Students Homework)
        // GET: /Admin/Enrollments
        public ActionResult Enrollments(string ddlSelectedName, string ddlSelectedDate)
        {
            //Display a list of courses enrollments
            var enrollments = sca.ShowAllEnrollments(); 

            //Create the drop down list of courses names. The selection enables to filter the students' list by course name
            var coursesNamesList = sca.GetCoursesNames();
            ViewBag.coursesList = new SelectList(coursesNamesList); //SelectList is the DataSource of a dropdown list

            //Create the drop down list of courses start dates.
            //The selection enables to filter the students' list by course start date
            var coursesDatesList = sca.GetCoursesDates();
            ViewBag.datesList = new SelectList(coursesDatesList);

            //In case all fields has a value - most "important" is ddlSelectedName, and then ddlSelectedDate.
            if (String.IsNullOrEmpty(ddlSelectedName))
            {
                if (String.IsNullOrEmpty(ddlSelectedDate))
                {
                    return View(enrollments);
                }
                else
                {
                    //ddlSelectedDate is not empty - ddlSelectedName is empty
                    return View(enrollments.Where(x => x.CourseStartDate.ToString("dd/MM/yyyy") == ddlSelectedDate));
                }
            }
            else
            {
                //ddlSelectedName is not empty
                return View(enrollments.Where(x => x.CourseName.ToString() == ddlSelectedName));
            }


        }
        #endregion
        #region Students List
        // GET: /Admin/AllStudents
        public ActionResult AllStudents(string sortOrder, string searchText,
            string currentFilter, int? page)
        {
            ViewBag.CurrentSort = sortOrder;

            //Ternary Operator- short way of "if"
            ViewBag.FullNameSort =
                sortOrder == "FullNameAsc" ? "FullNameDesc" : "FullNameAsc";
            ViewBag.EmailSort =
                sortOrder == "EmailAsc" ? "EmailDesc" : "EmailAsc";
            ViewBag.PhoneNumberSort =
                sortOrder == "PhoneNumAsc" ? "PhoneNumDesc" : "PhoneNumAsc";
            ViewBag.CourseNameSort =
                sortOrder == "CourseNameAsc" ? "CourseNameDesc" : "CourseNameAsc";
            ViewBag.CourseStartDateSort =
                sortOrder == "CourseStartDateAsc" ? "CourseStartDateDesc" : "CourseStartDateAsc";

            if (searchText != null)
            {
                page = 1;
            }
            else
            {
                searchText = currentFilter;
            }
            ViewBag.CurrentFilter = searchText; //Saves the filter and sorting while paging

            var stud = sca.DisplayAllStudents();

            if (!string.IsNullOrEmpty(searchText))
            {
                string searcTextUppercase = Utility.UpperCase(searchText);
                stud = stud.Where(s =>
                    s.FullName.Contains(searcTextUppercase) ||
                    s.Email.Contains(searchText) ||
                    s.PhoneNumber.Contains(searchText) ||
                    s.CourseName.ToString().Contains(searcTextUppercase) ||
                    s.CourseStartDate.ToString().Contains(searchText));
            }

            switch (sortOrder)
            {
                case "FullNameAsc":
                    stud = stud.OrderBy(s => s.FullName);
                    break;
                case "FullNameDesc":
                    stud = stud.OrderByDescending(s => s.FullName);
                    break;
                case "EmailAsc":
                    stud = stud.OrderBy(s => s.Email);
                    break;
                case "EmailDesc":
                    stud = stud.OrderByDescending(s => s.Email);
                    break;
                case "PhoneNumAsc":
                    stud = stud.OrderBy(s => s.PhoneNumber);
                    break;
                case "PhoneNumDesc":
                    stud = stud.OrderByDescending(s => s.PhoneNumber);
                    break;
                case "CourseNameAsc":
                    stud = stud.OrderBy(s => s.CourseName);
                    break;
                case "CourseNameDesc":
                    stud = stud.OrderByDescending(s => s.CourseName);
                    break;
                case "CourseStartDateAsc":
                    stud = stud.OrderBy(s => s.CourseStartDate);
                    break;
                case "CourseStartDateDesc":
                    stud = stud.OrderByDescending(s => s.CourseStartDate);
                    break;
                default:
                    stud = stud.OrderBy(s => s.StudentId);
                    break;
            }

            int pageSize = 5;
            int pageNumber = page ?? 1;
            return View(stud.ToPagedList(pageNumber, pageSize));
        }

        // GET: /Admin/StudentDetails/5
        public ActionResult StudentDetails(int? id)
        {
            if (sca.IsStudentDatabaseEmpty())
            {
                return View();
            }
            else
            {

                var stud = sca.DisplayAllStudents();
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                else
                {
                    if (stud.Any(p => p.StudentId == id))
                    {
                        return View(stud.Where(s => s.StudentId == id).SingleOrDefault());
                    }
                    else
                    {
                        return View(stud);
                    }
                }
            }
        }

        // GET: /Admin/DeleteStudent/5
        public ActionResult DeleteStudent(int? id)
        {
            //Display the desired student we would like to delete
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var student = sca.GetStudentById(id);
            if (student == null)
            {
                return HttpNotFound();
            }
            return View(student);
        }

        // POST: /Admin/DeleteStudent/5
        [HttpPost, ActionName("DeleteStudent")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            //The student should be removed from both USERS database and STUDENTS database
            if (sca.DeleteUserFromDB(id) && sca.DeleteStudent(id)) 
            {
                return RedirectToAction("AllStudents");
            }
            else
            {
                return RedirectToAction("DeleteStudent", id);
            }
        }
        #endregion
        #region Announcements

        // GET: /Admin/Announcements
        //Display the list of all the announcements that the admin has published
        public ActionResult Announcements(string sortOrder, string searchText,
            string currentFilter, int? page)
        {
            ViewBag.CurrentSort = sortOrder;

            //Ternary Operator- short way of "if"
            ViewBag.AnnouncementIdSort =
                sortOrder == "AnnouncementIdAsc" ? "AnnouncementIdDesc" : "AnnouncementIdAsc";
            ViewBag.CourseNumberSort =
                sortOrder == "CourseNumberAsc" ? "CourseNumberDesc" : "CourseNumberAsc";
            ViewBag.CourseNameSort =
                sortOrder == "CourseNameAsc" ? "CourseNameDesc" : "CourseNameAsc";
            ViewBag.StartDateSort =
                sortOrder == "StartDateAsc" ? "StartDateDesc" : "StartDateAsc";
            ViewBag.AnnouncementTitleSort =
                sortOrder == "AnnouncementTitleAsc" ? "AnnouncementTitleDesc" : "AnnouncementTitleAsc";
            ViewBag.AnnouncementDescriptionSort =
                sortOrder == "AnnouncementDescriptionAsc" ? "AnnouncementDescriptionDesc" : "AnnouncementDescriptionAsc";
            ViewBag.DateSort =
                sortOrder == "DateAsc" ? "DateDesc" : "DateAsc";

            if (searchText != null)
            {
                page = 1;
            }
            else
            {
                searchText = currentFilter;
            }
            ViewBag.CurrentFilter = searchText; //Saves the filter and sorting while paging

            var ann = sca.ShowAnnoucements();

            if (!string.IsNullOrEmpty(searchText))
            {
                string searcTextUppercase = Utility.UpperCase(searchText);
                ann = ann.Where(s =>
                    s.CourseNumber.ToString().Contains(searchText) ||
                    s.CourseName.ToString().Contains(searcTextUppercase) ||
                    s.StartDate.ToString().Contains(searchText) ||
                    s.AnnouncementTitle.ToString().Contains(searchText) ||
                    s.AnnouncementDescription.ToString().Contains(searchText) ||
                    s.Date.ToString().Contains(searchText)).ToList();
            }

            switch (sortOrder)
            {
                case "CourseNumberAsc":
                    ann = ann.OrderBy(s => s.CourseNumber);
                    break;
                case "CourseNumberDesc":
                    ann = ann.OrderByDescending(s => s.CourseNumber);
                    break;
                case "CourseNameAsc":
                    ann = ann.OrderBy(s => s.CourseName);
                    break;
                case "CourseNameDesc":
                    ann = ann.OrderByDescending(s => s.CourseName);
                    break;
                case "StartDateAsc":
                    ann = ann.OrderBy(s => s.StartDate);
                    break;
                case "StartDateDesc":
                    ann = ann.OrderByDescending(s => s.StartDate);
                    break;
                case "AnnouncementTitleAsc":
                    ann = ann.OrderBy(s => s.AnnouncementTitle);
                    break;
                case "AnnouncementTitleDesc":
                    ann = ann.OrderByDescending(s => s.AnnouncementTitle);
                    break;
                case "AnnouncementDescriptionAsc":
                    ann = ann.OrderBy(s => s.AnnouncementDescription);
                    break;
                case "AnnouncementDescriptionDesc":
                    ann = ann.OrderByDescending(s => s.AnnouncementDescription);
                    break;
                case "DateAsc":
                    ann = ann.OrderBy(s => s.Date);
                    break;
                case "DateDesc":
                    ann = ann.OrderByDescending(s => s.Date);
                    break;
                default:
                    ann = ann.OrderBy(s => s.AnnouncementId);
                    break;
            }

            int pageSize = 5;
            int pageNumber = page ?? 1;
            return View(ann.ToPagedList(pageNumber, pageSize));
        }
        // GET: /Admin/Create
        public ActionResult CreateNewAnnouncement()
        {
            //The admin should first select the desired course to publish an announcement
            ViewBag.CourseNames = new SelectList(sca.GetCourseName(), "CourseName", "CourseName".ToString());

            return View();
        }

        // POST: /Admin/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateNewAnnouncement([Bind(Include = "CourseName,StartDate,Title,Description")] 
            CreateAnnouncement announcement)
        {
            if (ModelState.IsValid)
            {
                sca.AddAnnouncement(announcement.CourseName, announcement.StartDate,
                announcement.Title, announcement.Description);

                return RedirectToAction("Announcements");
            }

            return View(announcement);
        }

        // GET: /Admin/Edit/5
        public ActionResult EditAnnouncment(int? id)
        {
            if (id == null)//In case the user has tried to navigate to the desired page and removed the id from the url
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var announcement = sca.FindAnnouncementById(id);
            if (announcement == null)
            {
                return HttpNotFound();
            }
            ViewBag.AnnouncementId = new SelectList(db.Announcements, "AnnouncementId", "AnnouncementId", announcement.AnnouncementId);
            return View(announcement);
        }

        // POST: /Admin/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditAnnouncment([Bind(Include = "AnnouncementId,AnnouncementTitle,AnnouncementDescription")] 
            ShowAnnouncements announcement)
        {
            if (ModelState.IsValid)
            {
                sca.EditAnnouncement(announcement.AnnouncementId,
                    announcement.AnnouncementTitle, announcement.AnnouncementDescription);
                return RedirectToAction("Announcements");
            }
            ViewBag.AnnouncementId = new SelectList(db.Announcements, "AnnouncementId", "AnnouncementId", announcement.AnnouncementId);
            return View(announcement);
        }

        // GET: /Admin/DeleteAnnouncement/5
        public ActionResult DeleteAnnouncement(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var announcement = sca.FindAnnouncementById(id);
            if (announcement == null)
            {
                return HttpNotFound();
            }
            return View(announcement);
        }

        // POST: /Admin/DeleteAnnouncement/5
        [HttpPost, ActionName("DeleteAnnouncement")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteAnnouncementConfirmed(int id)
        {
            if (sca.DeleteAnnouncement(id))
            {
                return RedirectToAction("Announcements");
            }
            else
            {
                return RedirectToAction("DeleteAnnouncement", id);
            }
        }
        #endregion

        #region StudentsHomework

        // GET: /Admin/StudentsHomework/5
        //Display the list of all the assignments that are relevant for the desired student, and their status
        public ActionResult StudentsHomework(int? id)
        {
            ViewBag.StudentName = sca.GetUserFullNameById(id);
            ViewBag.StudentId = id.ToString();
            
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var homework = sca.ShowStudentHomeworkToAdmin(id);
            if (homework == null)
            {
                return HttpNotFound();
            }
            return View(homework);
        }

        // POST: /Admin/StudentsHomeworkStatus/5
        //[HttpPost, ActionName("StudentsHomework")]
        //[ValidateAntiForgeryToken]
        public ActionResult DownloadHomework(int homeworkID, int assignmentID, int studentID)
        {
            //Each file that is downloaded automatically gets a name that is consisted of the properties below:
            string courseName = sca.GetCourseNameByHomeworkId(homeworkID);
            string startDate = sca.GetCourseStartDateByHomeworkId(homeworkID);
            string studentName = sca.GetUserFullNameByIdWithoutSpace(studentID);
            string assignment = "Assignment_" + assignmentID + "_";
            string sendDate = sca.GetHomeworkSendDate(homeworkID);

            //The folder to which the files are being saved after being uploaded by the student
            string folder = courseName + "_" + startDate; 
            string fileName = sca.GetFileName(homeworkID);

            string path = AppDomain.CurrentDomain.BaseDirectory + "UploadedFiles/" + folder + "/";
            byte[] fileBytes = System.IO.File.ReadAllBytes(path + fileName);

            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        // POST: /Admin/StudentsHomework/5 (FOR GRADE UPDATE)
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult StudentsHomework(string grade, int homeworkId, int studentId, int courseId)
        {
            int studentGrade = int.Parse(grade);
            if (ModelState.IsValid)
            {
                sca.UpdateGrade(homeworkId, DateTime.Today, studentGrade, studentId, courseId);
                return RedirectToAction("StudentsHomework");
            }
            return RedirectToAction("StudentsHomework");
        }

        #endregion

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
