﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.Data;
using System.Data.Entity;
using System.Net;
using PagedList;
using Newtonsoft.Json;


using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using MoodleIt.Logic;
using MoodleIt.Models;
using MoodleIt.ViewModels;

using System.IO;


namespace MoodleIt.Controllers
{
    [Authorize(Roles = "Student")] //Only a student user can access this controller
    public class StudentsController : Controller
    {
        UniversityDbContext db = new UniversityDbContext();
        StudentCourseActions sca = new StudentCourseActions();

        //
        // GET: /Students/MyProfile
        public ActionResult MyProfile(string currentUserId)
        {
            string currentUser = User.Identity.GetUserId(); //User ID of the current logged-in student
            int studId = sca.GetStudentId(currentUser); //Find the student Id by the current user id
            var stud = sca.GetStudentById(studId);

            return View(stud);
        }

        //***FUTURE OPTION FOR REGISTRATION TO MORE THAN ONE COURSE***:

        // GET: /Students/RegisterToCourse
        public ActionResult RegisterToCourse()
        {
            ViewBag.CourseNames = new SelectList(sca.GetCourseName(), "CourseName", "CourseName".ToString());

            return View();
        }

        // POST: /Students/RegisterToCourse
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RegisterToCourse([Bind(Include = "CourseName,CourseStartDate")] 
            Course course)
        {
            if (ModelState.IsValid)
            {
                int courseId = sca.GetCourseId(course.CourseName, course.CourseStartDate);
                string currentUser = User.Identity.GetUserId();
                int studentId = sca.GetStudentId(currentUser);
                sca.AddEnrollment(studentId, courseId);

                return RedirectToAction("MyProfile");
            }

            return View(course);
        }

        [HttpPost]
        public ActionResult GetCourseStartDate(string courseName) 
        {
            Courses x;
            switch (courseName)
            {
                case "AngularJS":
                    Enum.TryParse("AngularJS", out x);
                    break;
                case "CSS":
                    Enum.TryParse("CSS", out x);
                    break;
                default:
                    Enum.TryParse("HTML", out x);
                    break;
            }
            List<Course> dates = sca.GetCourseDate(x);
            var list = dates.Select(d => d.CourseStartDate).ToList();

            ViewBag.DateOptions = new SelectList(list, "CourseStartDate", "CourseStartDate");

            string result = JsonConvert.SerializeObject(list);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        //
        // GET: /Students/MyAssignmetns
        public ActionResult MyAssignmetns(string sortOrder, string searchText,
            string currentFilter, int? page)
        {
            string currentUser = User.Identity.GetUserId(); //User ID of the current logged-in student
            int studId = sca.GetStudentId(currentUser);           

            ViewBag.CurrentSort = sortOrder;

            //Ternary Operator- short way of "if"
            ViewBag.AssignmentIdSort =
                sortOrder == "AssignmentIdAsc" ? "AssignmentIdDesc" : "AssignmentIdAsc";
            ViewBag.CourseNumberSort =
                sortOrder == "CourseNumberAsc" ? "CourseNumberDesc" : "CourseNumberAsc";
            ViewBag.CourseNameSort =
                sortOrder == "CourseNameAsc" ? "CourseNameDesc" : "CourseNameAsc";
            ViewBag.StartDateSort =
                sortOrder == "StartDateAsc" ? "StartDateDesc" : "StartDateAsc";
            ViewBag.AssignmentTitleSort =
                sortOrder == "AssignmentTitleAsc" ? "AssignmentTitleDesc" : "AssignmentTitleAsc";
            ViewBag.DeadLineSort =
                sortOrder == "DeadLineAsc" ? "DeadLineDesc" : "DeadLineAsc";

            if (searchText != null)
            {
                page = 1;
            }
            else
            {
                searchText = currentFilter;
            }
            ViewBag.CurrentFilter = searchText; //Saves the filter and sorting while paging

            var assignments = sca.ShowStudentAssignments(studId);

            if (!string.IsNullOrEmpty(searchText))
            {
                assignments = assignments.Where(s =>
                    s.CourseNumber.ToString().Contains(searchText) ||
                    s.CourseName.ToString().Contains(searchText) ||
                    s.StartDate.ToString().Contains(searchText) ||
                    s.AssignmentTitle.ToString().Contains(searchText) ||
                    s.DeadLine.ToString().Contains(searchText)).ToList();
            }

            switch (sortOrder)
            {
                case "CourseNumberAsc":
                    assignments = assignments.OrderBy(s => s.CourseNumber);
                    break;
                case "CourseNumberDesc":
                    assignments = assignments.OrderByDescending(s => s.CourseNumber);
                    break;
                case "CourseNameAsc":
                    assignments = assignments.OrderBy(s => s.CourseName);
                    break;
                case "CourseNameDesc":
                    assignments = assignments.OrderByDescending(s => s.CourseName);
                    break;
                case "StartDateAsc":
                    assignments = assignments.OrderBy(s => s.StartDate);
                    break;
                case "StartDateDesc":
                    assignments = assignments.OrderByDescending(s => s.StartDate);
                    break;
                case "AssignmentTitleAsc":
                    assignments = assignments.OrderBy(s => s.AssignmentTitle);
                    break;
                case "AssignmentTitleDesc":
                    assignments = assignments.OrderByDescending(s => s.AssignmentTitle);
                    break;
                case "DeadLineAsc":
                    assignments = assignments.OrderBy(s => s.DeadLine);
                    break;
                case "DeadLineDesc":
                    assignments = assignments.OrderByDescending(s => s.DeadLine);
                    break;
                default:
                    assignments = assignments.OrderBy(s => s.AssignmentId);
                    break;
            }

            int pageSize = 5;
            int pageNumber = page ?? 1;
            return View(assignments.ToPagedList(pageNumber, pageSize));
        }

        // GET: /Students/AssignmentDetails
        public ActionResult AssignmentDetails(int? id)
        {
            var assignment = sca.GetAssignmentById(id);
            return View(assignment);
        }

        //
        // GET: /Students/MyHomework
        public ActionResult MyHomework(string currentUserId)
        {

            string currentUser = User.Identity.GetUserId(); //User ID of the current logged-in student
            int studId = sca.GetStudentId(currentUser);

            var hw = sca.ShowStudentHomework(studId);

            return View(hw);
        }

        // This action handles the form POST and the upload
        [HttpPost]
        public ActionResult MyHomeworkStatus(HttpPostedFileBase file, int assignmentID)
        {
            ViewBag.UploadFailed = "";

            //Gets only the extension of the file (.doc for example).
            string fileExtension = Path.GetExtension(file.FileName).ToLower();

            //The uploaded file will be saved with a name that is consisted of the properties below:
            string courseName = sca.GetCourseNameByAssignmentId(assignmentID);
            string startDate = sca.GetCourseStartDateByAssignmentId(assignmentID);
            string currentUser = User.Identity.GetUserId();
            int studentId = sca.GetStudentId(currentUser);
            string studentName = sca.GetUserFullNameByIdWithoutSpace(studentId);
            string sendDate = DateTime.Today.ToString("ddMMyyyy");
            string assignment = "Assignment_" + assignmentID + "_";

            string folder = courseName + "_" + startDate; //The file will be saved to this folder
            string fileName = folder + "_" + studentName + "_" + assignment + sendDate;

            var newfolder = Server.MapPath("~/UploadedFiles/" + folder); //New folder to create
            if (!Directory.Exists(newfolder)) //If the folder does not exist, then create it:
            {
                Directory.CreateDirectory(newfolder);
            }

            string path = Server.MapPath("~/UploadedFiles/" + folder + "/"); //The path in which the file is saved 

            int selectedFilesCount = Request.Files.Count; //Allow the upload only when ONE file is selected

            switch (selectedFilesCount)
            {
                case 1:
                    string[] allowExtensions = { ".doc", ".docx", ".pdf", ".rtf" }; //Only these extensions are allowed
                    string fileNameWithExtension = fileName + fileExtension;
                    if (allowExtensions.Contains(fileExtension))
                    {
                        try
                        {
                            //Save the file to the server
                            file.SaveAs(path + fileName + fileExtension);
                            if (sca.HomeworkExists(studentId, assignmentID)) 
                            {
                                //This method is used to update the desired homework instead of duplicate the file
                                sca.DeleteHomework(studentId, assignmentID);
                            }
                            //Add homework to the database
                            sca.UploadHomework(assignmentID, studentId, fileNameWithExtension);
                        }
                        catch (Exception ex)
                        {
                            ViewBag.UploadFailed = ex.Message;
                        }
                    }
                    else
                    {
                        ViewBag.UploadFailed = ViewBag.UploadFailed + "Illegal file format";
                    }
                    break;
                case 0:
                    ViewBag.UploadFailed = "No file was selected";
                    break;
                default:
                    ViewBag.UploadFailed = "Only one file is allowed";
                    break;
            }

            return RedirectToAction("MyHomework");
        }


        //
        // GET: /Students/ContactList
        public ActionResult ContactList(string sortOrder, string searchText,
            string currentFilter, int? page)
        {
            string currentUser = User.Identity.GetUserId();
            int studentId = sca.GetStudentId(currentUser);

            ViewBag.CurrentSort = sortOrder;

            //Ternary Operator- short way of "if"
            ViewBag.StudentIdSort =
                sortOrder == "StudentIdAsc" ? "StudentIdDesc" : "StudentIdAsc";
            ViewBag.CourseNumberSort =
                sortOrder == "CourseNumberAsc" ? "CourseNumberDesc" : "CourseNumberAsc";
            ViewBag.CourseNameSort =
                sortOrder == "CourseNameAsc" ? "CourseNameDesc" : "CourseNameAsc";
            ViewBag.FullNameSort =
                sortOrder == "FullNameAsc" ? "FullNameDesc" : "FullNameAsc";
            ViewBag.EmailSort =
                sortOrder == "EmailAsc" ? "EmailDesc" : "EmailAsc";
            ViewBag.PhoneNumberSort =
                sortOrder == "PhoneNumberAsc" ? "PhoneNumberDesc" : "PhoneNumberAsc";

            if (searchText != null)
            {
                page = 1;
            }
            else
            {
                searchText = currentFilter;
            }
            ViewBag.CurrentFilter = searchText; //Saves the filter and sorting while paging

            var list = sca.ContactList(studentId); //List of students who study with the current student

            if (!string.IsNullOrEmpty(searchText))
            {
                string searchTextUppercase = Utility.UpperCase(searchText);
                list = list.Where(s =>
                    s.CourseNumber.ToString().Contains(searchText) ||
                    s.CourseName.ToString().Contains(searchTextUppercase) ||
                    s.FullName.ToString().Contains(searchTextUppercase) ||
                    s.Email.ToString().Contains(searchText) ||
                    s.PhoneNumber.ToString().Contains(searchText)).ToList();
            }

            switch (sortOrder)
            {
                case "CourseNumberAsc":
                    list = list.OrderBy(s => s.CourseNumber);
                    break;
                case "CourseNumberDesc":
                    list = list.OrderByDescending(s => s.CourseNumber);
                    break;
                case "CourseNameAsc":
                    list = list.OrderBy(s => s.CourseName);
                    break;
                case "CourseNameDesc":
                    list = list.OrderByDescending(s => s.CourseName);
                    break;
                case "FullNameAsc":
                    list = list.OrderBy(s => s.FullName);
                    break;
                case "FullNameDesc":
                    list = list.OrderByDescending(s => s.FullName);
                    break;
                case "EmailAsc":
                    list = list.OrderBy(s => s.Email);
                    break;
                case "EmailDesc":
                    list = list.OrderByDescending(s => s.Email);
                    break;
                case "PhoneNumberAsc":
                    list = list.OrderBy(s => s.PhoneNumber);
                    break;
                case "PhoneNumberDesc":
                    list = list.OrderByDescending(s => s.PhoneNumber);
                    break;
                default:
                    list = list.OrderBy(s => s.StudentId);
                    break;
            }

            int pageSize = 5;
            int pageNumber = page ?? 1;
            return View(list.ToPagedList(pageNumber, pageSize));
        }

        //
        //GET: /Students/Announcements
        public ActionResult Announcements(string ddlSelectedName)
        {
            string currentUser = User.Identity.GetUserId();
            int studentId = sca.GetStudentId(currentUser);

            var list = sca.ShowCourseAnnouncements(studentId);         

            var coursesNamesList = sca.GetCoursesNamesById(studentId);
            ViewBag.coursesList = new SelectList(coursesNamesList); //SelectList is the DataSource of a dropdown list

            if (String.IsNullOrEmpty(ddlSelectedName))
            {

                return View(list);


            }
            else
            {
                //ddlSelectedName is not empty
                int courseId = sca.GetCourseIdByCourseName(ddlSelectedName, studentId);
                return View(list.Where(x => x.CourseId == courseId));
            }

            return View(list);
        }

        //
        //GET: /Students/Announcements
        public ActionResult AnnouncementDetails(int? id)
        {
            var ann = sca.ShowAnnoucements();
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            else
            {
                if (ann.Any(p => p.AnnouncementId == id))
                {


                    return View(ann.Where(s => s.AnnouncementId == id));
                }
                else
                {
                    return View(ann);
                }
            }

            return View(sca.ShowAnnoucements());
        }
    }
}