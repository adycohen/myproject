﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using MoodleIt.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MoodleIt.ViewModels
{
    public class ContactList
    {
        [Display(Name = "Student ID")]
        public int StudentId { get; set; }
        public int EnrollmentId { get; set; }
        public int CourseId { get; set; }
        [Display(Name = "Course Name")]
        public Courses CourseName { get; set; }
        [Display(Name = "Course Number")]
        public int CourseNumber { get; set; }
        [Display(Name = "Full Name")]
        public string FullName { get; set; }
        public string Email { get; set; }
        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }
    }
}