﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using MoodleIt.Models;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using PagedList;

namespace MoodleIt.ViewModels
{
    public class ShowHomework
    {
        [Display(Name = "Assignment ID")]
        public int AssignmentId { get; set; }

        [Display(Name = "Dead Line")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime DeadLine { get; set; }

        [Display(Name = "Is Homework Sent?")]
        [System.ComponentModel.DefaultValue("false")]
        public bool HomeworkSent { get; set; }

        [Display(Name = "Send Date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? SendDate { get; set; }

        [System.ComponentModel.DefaultValue("false")]
        [Display(Name = "Is Homework Checked?")]
        public bool HomeworkChecked { get; set; }

        [Display(Name = "Check Date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? CheckDate { get; set; }
        public int? Grade { get; set; }

        [Display(Name = "Homework ID")]
        public int? HomeworkId { get; set; }
        [Display(Name = "Student ID")]
        public int StudentId { get; set; }
        public int CourseId { get; set; }
    }
}