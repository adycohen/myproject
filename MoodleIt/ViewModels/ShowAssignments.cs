﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using MoodleIt.Models;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MoodleIt.ViewModels
{
    public class ShowAssignments
    {
        [Display(Name = "Assignment ID")]
        public int AssignmentId { get; set; }
        [Display(Name = "Course Number")]
        public int CourseNumber { get; set; }
        [Display(Name = "Course Name")]
        public Courses CourseName { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Start Date")]
        public DateTime StartDate { get; set; }
        [Display(Name = "Assignment Title")]
        public string AssignmentTitle { get; set; }
        [Display(Name = "Assignment Description")]
        public string AssignmentDesc { get; set; }
        [Display(Name = "Dead Line")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime DeadLine { get; set; }
        [Display(Name = "Course ID")]
        public int CourseId { get; set; }
    }
}