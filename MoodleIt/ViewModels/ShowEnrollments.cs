﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using MoodleIt.Models;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using PagedList;


namespace MoodleIt.ViewModels
{
    public class ShowEnrollments
    {
        [Display(Name = "Student ID")]
        public int StudentId { get; set; }
        [Display(Name = "Full Name")]
        public string FullName { get; set; }
        public string Email { get; set; }
        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }
        [Display(Name = "Course Name")]
        public Courses CourseName { get; set; }
        [Display(Name = "Course Start Date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yy}", ApplyFormatInEditMode = true)]
        public DateTime CourseStartDate { get; set; }

    }
}