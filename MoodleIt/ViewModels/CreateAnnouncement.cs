﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using MoodleIt.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MoodleIt.ViewModels
{
    public class CreateAnnouncement
    {
        [Display(Name = "Course Name")]
        public Courses CourseName { get; set; }

        [Display(Name = "Course Start Date")]
        public DateTime StartDate { get; set; }

        [Display(Name = "Annoucement Description")]
        public string Description { get; set; }
        public string Title { get; set; }
    }
}