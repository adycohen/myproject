﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using MoodleIt.Models;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using PagedList;

namespace MoodleIt.ViewModels
{
    public class ShowAnnouncements
    {
        [Display(Name = "Announcement ID")]
        public int AnnouncementId { get; set; }

        [Display(Name = "Course Number")]
        public int CourseNumber { get; set; }

        [Display(Name = "Course Name")]
        public Courses CourseName { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Course Start Date")]
        public DateTime StartDate { get; set; }

        [Display(Name = "Announcement Description")]
        [MaxLength(500)]
        public string AnnouncementDescription { get; set; }
        [Display(Name = "Title")]
        [MaxLength(30)]
        public string AnnouncementTitle { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }
    }
}

