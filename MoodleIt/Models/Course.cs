﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MoodleIt.Models
{
    public enum Courses
    {//Sample list of courses
        AngularJS = 10300,
        CSS = 10301,
        HTML = 10302
    }
    public class Course
    {
        public int CourseId { get; set; }
        [Display(Name = "Course Number")]
        public int CourseNumber { get; set; }
        [Display(Name = "Course Name")]
        public Courses CourseName { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Start Date")]
        public DateTime CourseStartDate { get; set; }

        public virtual ICollection<Enrollment> Enrollments { get; set; }
        public virtual ICollection<Assignment> Assignments { get; set; }
        public virtual ICollection<Announcement> Announcements { get; set; }

    }
}