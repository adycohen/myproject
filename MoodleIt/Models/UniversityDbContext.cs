﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace MoodleIt.Models
{
    public class UniversityDbContext : DbContext
    {
        public UniversityDbContext()
            : base("MoodleItDbContext")
        {
            Database.SetInitializer(new UniversityDbInitializier()); 
        }

        public DbSet<Student> Students { get; set; }
        public DbSet<Course> Courses { get; set; }
        public DbSet<Enrollment> Enrollments { get; set; }
        public DbSet<Homework> Homeworks { get; set; }
        public DbSet<Assignment> Assignments { get; set; }
        public DbSet<Announcement> Announcements { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}