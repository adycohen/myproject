﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace MoodleIt.Models
{
    public class UniversityDbInitializier : DropCreateDatabaseIfModelChanges<UniversityDbContext>
    {
        protected override void Seed(UniversityDbContext context) //This method is used to enter default data
        {
            //Courses
            var courses = new List<Course>
            {
                new Course{CourseName=Courses.AngularJS, CourseNumber = 10300, CourseStartDate=DateTime.Parse("01/01/2017")},
                new Course{CourseName=Courses.CSS, CourseNumber = 10301, CourseStartDate=DateTime.Parse("01/01/2018")},
                new Course{CourseName=Courses.HTML, CourseNumber = 10302, CourseStartDate=DateTime.Parse("02/02/2018")},
                new Course{CourseName=Courses.AngularJS, CourseNumber = 10300, CourseStartDate=DateTime.Parse("10/10/2017")}
            };
            courses.ForEach(s => context.Courses.Add(s));
            try
            {
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            //Assignments
            var assignment = new List<Assignment>
            {
                new Assignment{CourseId=1, AssignmentDescription="abc", AssignmentTitle="First Assignment",
                    DeadLine=DateTime.Parse("01/04/2019")},
                new Assignment{CourseId=2, AssignmentDescription="abcd", AssignmentTitle="First Assignment",
                    DeadLine=DateTime.Parse("02/04/2019")},
                new Assignment{CourseId=3, AssignmentDescription="abcde", AssignmentTitle="First Assignment",
                    DeadLine=DateTime.Parse("02/04/2019")},
                new Assignment{CourseId=4, AssignmentDescription="abcdef", AssignmentTitle="First Assignment",
                    DeadLine=DateTime.Parse("02/05/2019")},
                new Assignment{CourseId=1, AssignmentDescription="abcdefg", AssignmentTitle="Second Assignment",
                    DeadLine=DateTime.Parse("02/06/2019")},
                new Assignment{CourseId=2, AssignmentDescription="abcdefgh", AssignmentTitle="Second Assignment",
                    DeadLine=DateTime.Parse("02/07/2019")}
            };
            assignment.ForEach(a => context.Assignments.Add(a));
            try
            {
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            //Announcements
            var announcement = new List<Announcement>
            {
                new Announcement{CourseId=1, AnnouncementTitle="Hello", AnnouncementDescription="...", Date=DateTime.Today},
                new Announcement{CourseId=2, AnnouncementTitle="Hello", AnnouncementDescription="...", Date=DateTime.Today},
                new Announcement{CourseId=3, AnnouncementTitle="Hello", AnnouncementDescription="...", Date=DateTime.Today},
                new Announcement{CourseId=4, AnnouncementTitle="Hello :)", AnnouncementDescription="...", Date=DateTime.Today}

            };
            announcement.ForEach(x => context.Announcements.Add(x));
            try
            {
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }
    }
}