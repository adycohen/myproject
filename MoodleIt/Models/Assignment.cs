﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MoodleIt.Models
{
    public class Assignment //This table contains the data of the assignments given by the teacher
    {
        [Display(Name = "Assignment ID")]
        public int AssignmentId { get; set; }
        [Display(Name = "Course ID")]
        public int CourseId { get; set; }
        [Display(Name = "Assignment Title")]
        [MaxLength(30)]
        public string AssignmentTitle { get; set; }
        [DataType(DataType.MultilineText)]
        [Display(Name = "Assignment Description")]
        [MaxLength(500)]
        public string AssignmentDescription { get; set; }
        [Display(Name = "Dead Line")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime DeadLine { get; set; }

        public virtual Course Course { get; set; }
        public virtual ICollection<Homework> Homeworks { get; set; }
    }
}