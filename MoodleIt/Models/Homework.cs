﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MoodleIt.Models
{
    public class Homework //This table contains the data of homework uploaded by students
    {
        [Display(Name = "Homework ID")]
        public int HomeworkId { get; set; }
        [Display(Name = "Student ID")]
        public int StudentId { get; set; }
        [Display(Name = "Assignment ID")]
        public int AssignmentId { get; set; }
        public string FilePath { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "dd/MM/yyyy")]
        [Display(Name = "Send Date")]
        public DateTime SendDate { get; set; }
        [System.ComponentModel.DefaultValue("false")]
        [Display(Name = "Homework sent?")]
        public bool HomeworkSent { get; set; }
        [System.ComponentModel.DefaultValue("false")]
        [Display(Name = "Homework Checked?")]
        public bool HomeworkChecked { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "dd/MM/yyyy")]
        [Display(Name = "Check Date")]
        public DateTime? CheckDate { get; set; }
        public int? Grade { get; set; }

        public virtual Assignment Assignment { get; set; }
        public virtual Student Student { get; set; }
    }
}