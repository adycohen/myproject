﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MoodleIt.Models
{
    public class Enrollment
    {
        public int EnrollmentId { get; set; }
        [Display(Name = "Student ID")]
        public int StudentId { get; set; }
        public int CourseId { get; set; }


        public virtual Student Student { get; set; }
        public virtual Course Course { get; set; }
    }
}