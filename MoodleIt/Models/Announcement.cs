﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MoodleIt.Models
{
    public class Announcement
    {
        [Display(Name = "Announcment ID")]
        public int AnnouncementId { get; set; }
        public int CourseId { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "Announcement")]
        [MaxLength(500)]
        public string AnnouncementDescription { get; set; }
        [Display(Name = "Title")]
        [MaxLength(30)]
        public string AnnouncementTitle { get; set; }

        [Display(Name = "Date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }

        public virtual Course Course { get; set; }

    }
}