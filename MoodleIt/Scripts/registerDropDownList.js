﻿$(document).ready(function () {
    $("#CourseName").change(function () {
        var courseName = $(this).val();
        debugger
        $.ajax({
            url: "/Account/GetCourseStartDate",
            type: "POST",
            datatype: "application/json",
            contentType: "application/json",
            data: JSON.stringify({
                courseName: courseName
            }),
            success: function (result) {
                debugger
                $("#CourseStartDate").html("");
                $("#CourseStartDate").append
                ($("<option></option>").val(null).html("-- Select Course Start Date --"));
                $.each(JSON.parse(result), function (i, date) {
                    var year = date.slice(0, 4);
                    var month = date.slice(5, 7);
                    var day = date.slice(8, 10);
                    var fullDate = day.concat("/", month, "/", year);
                    $("#CourseStartDate").append
                    ($("<option></option>").val(fullDate).html(fullDate))
                });


            },
            error: function () {
                debugger
                alert("something went wrong");
            }
        });
    });
});