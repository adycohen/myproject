<p>
    <i>Moodle It</i> is a web application, that has been created to enable communication between students
    and teachers.
    <br />
    The teacher (admin) can:
    <ul>
        <li>Publish announcements and assignments.</li>
        <li>Open new courses of AngularJS, CSS and HTML.</li>
        <li>Manage the students' database.</li>
        <li>Check homework and grade.</li>
    </ul>
    The student can:
    <ul>
        <li>Upload homework.</li>
        <li>Read the announcements from the teacher.</li>
        <li>Get details about other students in his class.</li>
    </ul>
    Tech Stack:
    <ol>
        <li>ASP.NET MVC</li>
        <li>Entity Framework</li>
        <li>AngularJS</li>
        <li>C#</li>
    </ol>
</p>